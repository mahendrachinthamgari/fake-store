let form = document.querySelector("form");

function handleSubmit(event) {
    event.preventDefault();

    let count = 0;

    let firstName = event.target.elements.firstname;
    let lastName = event.target.elements.lastname;
    let email = event.target.elements.email;
    let password = event.target.elements.password;
    let repeatPassword = event.target.elements.repeatpassword;
    let checkBox = event.target.elements.checkbox;

    if (validateFirstName(firstName)) {
        firstName.parentElement.classList.add("sucess");
        firstName.parentElement.classList.remove("error");
        firstName.nextElementSibling.style.display = "none";
        count++;
    } else {
        firstName.parentElement.classList.add("error");
        firstName.parentElement.classList.remove("sucess");
        firstName.nextElementSibling.style.display = "block";
    }
    if (validateLasttName(lastName)) {
        lastName.parentElement.classList.add("sucess");
        lastName.parentElement.classList.remove("error");
        lastName.nextElementSibling.style.display = "none";
        count++;
    } else {
        lastName.parentElement.classList.add("error");
        lastName.parentElement.classList.remove("sucess");
        lastName.nextElementSibling.style.display = "block";
    }
    if (validateEmail(email)) {
        email.parentElement.classList.add("sucess");
        email.parentElement.classList.remove("error");
        email.nextElementSibling.style.display = "none";
        count++;
    } else {
        email.parentElement.classList.add("error");
        email.parentElement.classList.remove("sucess");
        email.nextElementSibling.style.display = "block";
    }
    if (validatePassword(password)) {
        password.parentElement.classList.add("sucess");
        password.parentElement.classList.remove("error");
        password.nextElementSibling.style.display = "none";
        count++;
    } else {
        password.parentElement.classList.add("error");
        password.parentElement.classList.remove("sucess");
        password.nextElementSibling.style.display = "block";
    }
    if (validateRepeatPassword(repeatPassword, password)) {
        repeatPassword.parentElement.classList.add("sucess");
        repeatPassword.parentElement.classList.remove("error");
        repeatPassword.nextElementSibling.style.display = "none";
        count++;
    } else {
        repeatPassword.parentElement.classList.add("error");
        repeatPassword.parentElement.classList.remove("sucess");
        repeatPassword.nextElementSibling.style.display = "block";
    }
    if (checkBox.checked) {
        let checkBoxParent = checkBox.parentElement;
        count++;
        checkBoxParent.nextElementSibling.style.display = "none";

    } else {
        let checkBoxParent = checkBox.parentElement;
        checkBoxParent.nextElementSibling.textContent = "Please click on the box";
        checkBoxParent.nextElementSibling.style.display = "block";
    }

    if (count === 6) {
        form.nextElementSibling.style.display = "block";
    } else {
        form.nextElementSibling.style.display = "none";
    }
}

form.addEventListener("submit", handleSubmit);


function validateFirstName(firstName) {
    let trimmedFirstName = firstName.value.trim();
    let firstNameError = "";
    if (trimmedFirstName === "" || checkForNumber(trimmedFirstName) || checkForSpecialCharacter(firstName.value)) {
        firstNameError = "Can't be empty and must contains only alphabets";
        firstName.nextElementSibling.textContent = firstNameError;
        return false;
    } else {
        return true;
    }
}

function validateLasttName(lastName) {
    let trimmedlastName = lastName.value.trim();
    let lastNameError = "";
    if (trimmedlastName === "" || checkForNumber(trimmedlastName) || checkForSpecialCharacter(trimmedlastName)) {
        lastNameError = "Can't be empty and must contains only alphabets";
        lastName.nextElementSibling.textContent = lastNameError;
        return false;
    } else {
        return true;
    }
}

function validateEmail(email) {
    let emailError = "";
    if (email.value === "" || email.value.includes('@') === false || email.value.includes('.') === false || (/[`!#$%^&*()_+\-=\[\]{};':"\\|,<>\/?~]/).test(email.value)) {
        emailError = "Invalid email";
        email.nextElementSibling.textContent = emailError;
        return false;
    } else {
        return true;
    }
}

function validatePassword(password) {
    let passwordError = "";
    if (password.value.length < 7 || (/[A-Z]/.test(password.value)) === false || (/[a-z]/.test(password.value)) === false || checkForSpecialCharacter(password.value) === false) {
        passwordError = "Password length must be greater than 6 characters and Must contain at least one upper case letter and lower case letter and special character [@#$%^&*()!~`,.]";
        password.nextElementSibling.textContent = passwordError;
        return false
    } else {
        return true;
    }
}

function validateRepeatPassword(repeatPassword, password) {
    let repeatPasswordError = "";
    if (repeatPassword.value !== password.value) {
        repeatPasswordError = "Password mismatched";
        repeatPassword.nextElementSibling.textContent = repeatPasswordError;
        return false;
    } else if (repeatPassword.value === "") {
        repeatPasswordError = "Can't be empty";
        repeatPassword.nextElementSibling.textContent = repeatPasswordError;
        return false;
    }
    else {
        return true;
    }
}

function checkForNumber(value) {
    return (/[0-9]/.test(value));
}

function checkForSpecialCharacter(value) {
    return (/[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/).test(value);
}
