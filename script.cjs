const loader = document.querySelector('.lds-spinner');
const loaderContainer = document.querySelector('.loader-container');

fetch('https://fakestoreapi.com/products')
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        if (data) {
            console.log("sucess")
            fakeStore(data);
            loader.classList.remove('lds-spinner');
            loaderContainer.classList.remove('loader-container');
        } else {
            loader.classList.remove('lds-spinner');
            loaderContainer.classList.remove('loader-container');
            fetchSucessZeroData();
        }
    })
    .catch((err) => {
        console.error("Error occured");
        console.error(err);
        loader.classList.remove('lds-spinner');
        loaderContainer.classList.remove('loader-container');
        errorFunctionCall();
    });

function fakeStore(data) {

    const header = document.querySelector('header');
    const footer = document.querySelector('footer');
    const ul = document.querySelector('ul');

    header.style.display = 'block';
    footer.style.display = 'block';

    if (Array.isArray(data)) {
        data.forEach((product) => {
            let li = document.createElement('li');
            let img = document.createElement('img');
            let h2 = document.createElement('h2');
            let p = document.createElement('p');
            let button = document.createElement('button');
            let price = document.createElement('h3');
            let div = document.createElement('div');
            let button2 = document.createElement('button');
            let div2 = document.createElement("div");
            let div3 = document.createElement('div');
            let span = document.createElement('span');
            let div4 = document.createElement('div');
            let category = document.createElement('h4');

            button.setAttribute('class', 'buy-now');
            div.setAttribute('class', 'container');
            button2.setAttribute('class', 'rating-button');
            div2.setAttribute('class', 'price-rating-container');
            div3.setAttribute('class', 'content-container');
            div4.setAttribute('class', 'rating');

            category.innerText = product.category;
            img.src = product.image;
            h2.innerText = product.title;
            p.innerText = product.description;
            button.innerText = 'Add to cart';
            button2.innerHTML = product.rating.rate + ' ' + '<i class="fa-regular fa-star"></i>';
            price.innerText = '$' + product.price;
            span.innerText = '(' + product.rating.count + ')';

            div4.append(button2, span);
            div2.append(div4, price);
            div.append(div2, button);
            div3.append(img, h2, p, category);

            li.append(div3, div);
            ul.append(li);
        });
    } else {
        let dataArray = [];
        dataArray.push(data);
        fakeStore(dataArray)
    }
}


function errorFunctionCall() {

    const ul = document.querySelector('ul');

    let divError = document.createElement('div');
    let h2 = document.createElement('h2');

    divError.setAttribute('class', 'error');

    h2.innerText = 'Fetch faild, please try again!';

    divError.append(h2);
    ul.append(divError);

}

function fetchSucessZeroData() {
    const ul = document.querySelector('ul');

    let divZerodata = document.createElement('div');
    let h2 = document.createElement('h2');

    divZerodata.setAttribute('class', 'zero-data');

    h2.innerText = 'Nothing to show!';

    divZerodata.append(h2);
    ul.append(divZerodata);

}